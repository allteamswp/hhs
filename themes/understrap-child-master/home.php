<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package understrap
 */

get_header();

$container   = get_theme_mod( 'understrap_container_type' );
$page_for_posts = get_option( 'page_for_posts' );
//print_r($page_for_posts);
$posts_setting = get_post( $page_for_posts ); 
$title = $posts_setting->post_title;

?>
<div class="wrapper page-leftside" id="page-wrapper">

	<div class="<?php echo esc_attr( $container ); ?>" id="content">

		<div class="row">

			<?php get_sidebar( 'left' ); ?>

			<div class="<?php if ( is_active_sidebar( 'left-sidebar' ) ) : ?>col-md-9<?php else : ?>col-md-12<?php endif; ?> content-area"
				id="primary">
				<h1 class="entry-title"><?php echo $title; ?></h1> 
				<main class="site-main" id="main" role="main">

					<?php while ( have_posts() ) : the_post(); ?>

						<?php get_template_part( 'loop-templates/content', '' ); ?>

						

					<?php endwhile; // end of the loop. ?>

				</main><!-- #main -->

			</div><!-- #primary -->
            
            <?php dynamic_sidebar( 'undercontentfull' ); ?>

		</div><!-- .row -->
        

	</div><!-- Container end -->

</div><!-- Wrapper end -->

<?php get_footer(); ?>
