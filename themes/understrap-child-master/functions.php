<?php

function understrap_remove_scripts() {
    wp_dequeue_style( 'understrap-styles' );
    wp_deregister_style( 'understrap-styles' );

    wp_dequeue_script( 'understrap-scripts' );
    wp_deregister_script( 'understrap-scripts' );

    // Removes the parent themes stylesheet and scripts from inc/enqueue.php
}
add_action( 'wp_enqueue_scripts', 'understrap_remove_scripts', 20 );

add_action( 'wp_enqueue_scripts', 'theme_enqueue_styles' );
function theme_enqueue_styles() {

	// Get the theme data
	$the_theme = wp_get_theme();

    wp_enqueue_style( 'child-understrap-styles', get_stylesheet_directory_uri() . '/css/child-theme.css', array(), $the_theme->get( 'Version' ) );
    wp_enqueue_style( 'child-custom-css', get_stylesheet_directory_uri() . '/css/custom-css.css', array(), $the_theme->get( 'Version' ) );
    wp_enqueue_script( 'jquery' );
	wp_enqueue_script( 'popper-scripts', get_template_directory_uri() . '/js/popper.min.js', array(), false);
    wp_enqueue_script( 'child-understrap-scripts', get_stylesheet_directory_uri() . '/js/child-theme.min.js', array(), $the_theme->get( 'Version' ), true );
}


add_filter( 'theme_page_templates', 'child_theme_remove_page_template' );
/**
* Remove page templates inherited from the parent theme.
*
* @param array $page_templates List of currently active page templates.
*
* @return array Modified list of page templates.
*/
function child_theme_remove_page_template( $page_templates ) {
    // Remove the template we don�t need.
    unset( $page_templates['page-templates/blank.php'] );
    unset( $page_templates['page-templates/both-sidebarspage.php'] );
    unset( $page_templates['page-templates/empty.php'] );
    unset( $page_templates['page-templates/fullwidthpage.php'] );
    unset( $page_templates['page-templates/left-sidebarpage.php'] );
    unset( $page_templates['page-templates/right-sidebarpage.php'] );
    return $page_templates;
}

if ( ! function_exists( 'understrap_child_widgets_init' ) ) {
	/**
	 * Initializes themes widgets.
	 */
	function understrap_child_widgets_init() {
	
		register_sidebar( array(
			'name'          => __( 'UnderContent Full', 'understrap' ),
			'id'            => 'undercontentfull',
			'description'   => 'Widget area below main content and above footer',
		    'before_widget'  => '<div id="%1$s" class="footer-widget %2$s col-md-12">', 
		    'after_widget'   => '</div><!-- .footer-widget -->', 
		    'before_title'   => '<h3 class="widget-title">', 
		    'after_title'    => '</h3>', 
		) );

	}
} // endif function_exists( 'understrap_widgets_init' ).
add_action( 'widgets_init', 'understrap_child_widgets_init' );
function allteams_excerpts() {
	global $post;
	//$mycontent = $post->post_excerpt;
	$mycontent = $post->post_content;
	$mycontent = strip_shortcodes($mycontent);
	$mycontent = str_replace(']]>', ']]&gt;',$mycontent);
	$mycontent = strip_tags($mycontent);
	$excerpt_length = 55;
	$words = explode(' ', $mycontent,$excerpt_length + 1);
	if(count($words) > $excerpt_length) :
		array_pop($words);
		array_push($words, '...');
		$mycontent = implode(' ', $words);
	endif;
	
	// Make sure to return the content
	return $mycontent;
}