<?php
/**
 * Template Name: Frontpage
 *
 * Template for displaying a page without sidebar even if a sidebar widget is published.
 *
 * @package understrap
 */

get_header();
$container = get_theme_mod( 'understrap_container_type' );
?>

<div class="wrapper" id="full-width-page-wrapper">

	<div class="<?php echo esc_attr( $container ); ?>" id="content">

		<div class="row">

			<div class="col-md-12 content-area" id="primary">

				<main class="site-main" id="main" role="main">

					<?php while ( have_posts() ) : the_post(); ?>

						<article <?php post_class(); ?> id="post-<?php the_ID(); ?>">

                        
                        	<?php echo get_the_post_thumbnail( $post->ID, 'full' ); ?>
                        
                        	<div class="entry-content">
                        
                        		<?php the_content(); ?>
                        
                        		<?php
                        		wp_link_pages( array(
                        			'before' => '<div class="page-links">' . __( 'Pages:', 'understrap' ),
                        			'after'  => '</div>',
                        		) );
                        		?>
                        
                        	</div><!-- .entry-content -->
                        
                        	
                        
                        </article><!-- #post-## -->

					

					<?php endwhile; // end of the loop. ?>

				</main><!-- #main -->

			</div><!-- #primary -->

		</div><!-- .row end -->

	</div><!-- Container end -->

</div><!-- Wrapper end -->

<?php get_footer(); ?>
